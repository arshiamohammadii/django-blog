from django import forms



class ShareForm(forms.Form):
    name = forms.CharField(label='Your Name', max_length=100)
    email = forms.EmailField(label='Email address', required=True)
    to = forms.EmailField(label='Recipient Email address', required=True)
    comment = forms.CharField(label='Anything you want to add', widget=forms.Textarea)
    
