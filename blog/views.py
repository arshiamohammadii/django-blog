from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.views.generic import ListView
from django.core.mail import send_mail
from .models import Post
from .forms import ShareForm

# Create your views here.
# def post_list(request):
#     post_list = Post.objects.all()
#     paginator = Paginator(post_list, 3)
#     page_number = request.GET.get("page")
#     page = paginator.get_page(page_number)
#     return render(request, 'blog/post/list.html',
#             context={'post_list': post_list, 'page': page})
class PostListView(ListView):
    paginate_by = 3
    queryset = Post.published.all()
    context_object_name = 'posts'
    template_name = 'blog/post/list.html'



def post_detail(request, year, month, day, slug):
    post = get_object_or_404(Post, published_at__year=year, published_at__month=month,
                        published_at__day=day, slug=slug)
    return render(request, 'blog/post/detail.html',
                    context={'post': post})


def post_share(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    sent = False
    if request.method == "POST":
        form = ShareForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            sender = form.cleaned_data['email']
            send_to = form.cleaned_data['to']
            comment = form.cleaned_data['comment']
            send_mail('webiste testing', f'Hello {name}, {settings.SITE_URL}{post.get_absolute_url()}\n Also added: {comment}', sender, [send_to])
            sent = True
    form = ShareForm()
    return render(request, 'blog/post/share.html', context={"post": post,
                                                                "form": form})