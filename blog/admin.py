from django.contrib import admin
from .models import Post
# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_display = ["title", "author", "slug", "published_at", "status"]
    list_filter = ["status", "created_at", "published_at", "author"]
    search_fields = ["title", "body"]
    prepopulated_fields = {"slug": ["title"]}
    ordering = ["status", "published_at"]
    date_hierarchy = "published_at"
    raw_id_fields = ["author"]



admin.site.register(Post, PostAdmin)
