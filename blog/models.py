from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse

#custom managers here
class PublishedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status='PB')

# Create your models here.
class Post(models.Model):
    class Status(models.TextChoices):
        DRAFT = 'DF', 'Draft'
        PUBLISHED = 'PB', 'PUBLISHED'

    title = models.CharField(max_length=255)
    body = models.TextField()
    published_at = models.DateTimeField(default=timezone.now)
    slug = models.SlugField(max_length=255, unique_for_date='published_at')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=2, 
                    choices=Status.choices, default=Status.DRAFT)
    author = models.ForeignKey(to=User, null=True,
                    on_delete=models.CASCADE, related_name='posts')
    
    objects = models.Manager()
    published = PublishedManager()
    class Meta:
        ordering = ["-published_at"]
        indexes = [models.Index(fields=["published_at"])]

    def get_absolute_url(self):
        return reverse('detail', kwargs={'year': self.published_at.year,
                        'month': self.published_at.month, 'day':self.published_at.day,
                        'slug': self.slug})
    
    def __str__(self):
        return self.title