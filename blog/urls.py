from . import views
from django.urls import path


urlpatterns = [
    path("", views.PostListView.as_view(), name="index"),
    path("<int:year>/<int:month>/<int:day>/<slug:slug>", views.post_detail, name="detail"),
    path("<int:post_id>/share", views.post_share, name='post_share')
]